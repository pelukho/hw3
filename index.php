<?php

declare(strict_types=1);

require_once __DIR__ . '/autoload.php';

use App\AnotherEmployee\Employee as AnotherEmployee;
use App\Employee\Employee;
use \App\Employee11\Employee as E11;
use App\Student\Student;
use App\Towns\City;
use App\User\User;
use App\User61\User as User6;
use \App\User14\User as FIO;

$employee  = new Employee();

$employee->name = 'Иван';
$employee->age = 25;
$employee->salary = 100;

$employee2 = new Employee();

$employee2->name = 'Вася';
$employee2->age = 26;
$employee2->salary = 2000;

echo $employee->salary + $employee2->salary . "<br />";
echo $employee->age + $employee2->age;

echo "<hr> <br />";

$anotherEmployee = new AnotherEmployee();

$anotherEmployee->name = 'Dimon';
$anotherEmployee->age = 18;
$anotherEmployee->salary = 1500;

$anotherEmployee2 = new AnotherEmployee();

$anotherEmployee2->name = 'Kolka';
$anotherEmployee2->age = 28;
$anotherEmployee2->salary = 2500;

echo $anotherEmployee->getSalary() + $anotherEmployee2->getSalary();

echo "<hr> <br />";

$user = new User();

$user->name = 'Коля';
$user->age = 25;
$user->setAge(30);

echo $user->age;

echo "<hr> <br />";

$user6 = new User6();

$user6->setAge(25);
$user6->addAge(5);
$user6->subAge(10);

echo $user6->age;

echo "<hr> <br />";

$emp1 = new E11('Вася',  25,  1000);
$emp2 = new E11('Петя',  30,  2000);

printf('ЗП Васи: %d; ЗП Пети: %d', $emp1->salary, $emp2->salary);

echo "<hr> <br />";

$city = new City('Kyiv', '482', 2950800);

$props = array('name', 'foundation', 'population');
foreach ($props as $prop) {
    echo $city->$prop . '<br />';
}

echo "<hr> <br />";

$methods = ['method1' => 'getName', 'method2' => 'getAge'];

$emp13 = new AnotherEmployee();
$emp13->name = 'Petro';
$emp13->salary = 10000;
$emp13->age = 33;

echo $emp13->getAge() . '<br />';

foreach ($methods as $method) {
    echo $emp13->$method() . '<br />';
}

echo "<hr> <br />";

$user14 = new FIO();

echo $user14
    ->setSurname('Pelukho')
    ->setName('Serhii')
    ->setPatronymic('Serhiiovich')
    ->getFullName();

echo "<hr> <br />";

$newEmployee1 = new Employee();
$newEmployee2 = new Employee();
$newEmployee3 = new Employee();

$newEmployee1->name = 'Kolia';
$newEmployee2->name = 'Dima';
$newEmployee3->name = 'Sasha';
$newEmployee1->salary = 1000;
$newEmployee2->salary = 2000;
$newEmployee3->salary = 3000;

$sumSalary = 0;
$arr = [$newEmployee1, $newEmployee2, $newEmployee3];

foreach ($arr as $employee) {
    echo $employee->name . '<br>';
    $sumSalary += $employee->salary;
}

$newStudent1 = new Student();
$newStudent2 = new Student();
$newStudent3 = new Student();

$newStudent1->name = 'Kostik';
$newStudent2->name = 'Arsen';
$newStudent3->name = 'Lupen';
$newStudent1->scholarship = 100;
$newStudent2->scholarship = 200;
$newStudent3->scholarship = 300;

$sumScholarship = 0;
$arr = [$newStudent1, $newStudent2, $newStudent3];

foreach ($arr as $student) {
    echo $student->name . '<br>';
    $sumScholarship += $student->scholarship;
}

printf('Salary sum: %d, Scholarship sum: %d', $sumSalary, $sumScholarship);
