<?php

declare(strict_types=1);

namespace App\Ecommerce;

use function array_reduce;

class Cart
{
    public array $products = [];

    /**
     * Add product
     *
     * @param Product $product
     *
     * @return Cart
     */
    public function add(Product $product): Cart
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product by name
     *
     * @param string $name
     *
     * @return Cart
     */
    public function remove(string $name): Cart
    {
        if (!empty($this->products)) {
            foreach ($this->products as $key => $product) {
                if ($name === $product->name) {
                    unset($this->products[$key]);
                }
            }
        }

        return $this;
    }

    /**
     * Return total cost
     *
     * @return float|null
     */
    public function getTotalCost(): ?float
    {
        if (!empty($this->products)) {
            return array_reduce($this->products, function ($sum, $product) {
                $sum += $product->price;

                return $sum;
            });
        } else {
            return null;
        }
    }

    /**
     * Return total quantity
     * @return int|null
     */
    public function getTotalQuantity(): ?int
    {
        if (!empty($this->products)) {
            return array_reduce($this->products, function ($sum, $product) {
                $sum += $product->quantity;

                return $sum;
            });
        } else {
            return null;
        }
    }

    /**
     * @return float|null
     */
    public function getAvgPrice(): ?float
    {
        if (!empty($this->products)) {
            return $this->getTotalCost() / count($this->products);
        } else {
            return null;
        }
    }
}
