<?php

declare(strict_types=1);

namespace App\Ecommerce;

class Product
{
    private string $name;
    private float $price;
    private int $quantity;

    public function __construct(string $name, float $price, int $quantity)
    {
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    /**
     * Making property only for reading
     *
     * @param $prop
     *
     * @return mixed
     */
    public function __get($prop)
    {
        return $this->$prop;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->price * $this->quantity;
    }
}
