<?php

declare(strict_types=1);

namespace App\User;

class User
{
    public string $name;
    public int $age;

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        if ($age >= 18) {
            $this->age = $age;
        }
    }
}
