<?php

declare(strict_types=1);

namespace App\Manager;

class Employee
{
    public string $name;
    public float $salary;

    /**
     * @return float
     */
    public function doubleSalary(): float
    {
        return $this->salary * 2;
    }
}
