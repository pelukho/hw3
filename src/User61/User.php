<?php

declare(strict_types=1);

namespace App\User61;

class User
{
    public string $name;
    public ?int $age = null;

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        if ($this->isValidAge($age)) {
            $this->age = $age;
        }
    }

    /**
     * @param int $years
     *
     */
    public function addAge(int $years)
    {
        $this->setAge($this->age + $years);
    }

    /**
     * @param int $years
     *
     */
    public function subAge(int $years)
    {
        $this->setAge($this->age - $years);
    }

    /**
     *
     * @param int $years
     *
     * @return bool
     */
    private function isValidAge(int $years): bool
    {
        return  $years > 0 ? true : false;
    }
}
