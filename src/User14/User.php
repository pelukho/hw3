<?php

declare(strict_types=1);

namespace App\User14;

class User
{
    private string $name;
    private string $surname;
    private string $patronymic;

    /**
     * @param string $name
     *
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $surname
     *
     * @return User
     */
    public function setSurname(string $surname): User
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @param string $patronymic
     *
     * @return User
     */
    public function setPatronymic(string $patronymic): User
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    public function getFullName(): string
    {
        return substr($this->surname, 0, 1) .
            substr($this->name, 0, 1) .
            substr($this->patronymic, 0, 1);
    }
}