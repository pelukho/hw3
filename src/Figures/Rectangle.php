<?php

declare(strict_types=1);

namespace App\Figures;

class Rectangle
{
    public float $h;
    public float $w;

    /**
     * Get squire of the rectangle
     *
     * @return float
     */
    public function getSquare (): float
    {
        return $this->h * $this->w;
    }

    /**
     * Get perimeter of the rectangle
     *
     * @return float
     */
    public function getPerimeter (): float
    {
        return ($this->w + $this->h) * 2;
    }
}
