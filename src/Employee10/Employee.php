<?php

declare(strict_types=1);

namespace App\Employee10;

class Employee
{
    private string $name;
    private string $surname;
    public float $salary;

    /**
     * Making name and surname only for reading
     *
     * @param $name
     *
     * @return string
     */
    public function __get($name): string
    {
        return $this->$name;
    }
}