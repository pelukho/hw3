<?php

declare(strict_types=1);

namespace App\Employee;

class Employee
{
    public string $name;
    public int $age;
    public float $salary;
}
