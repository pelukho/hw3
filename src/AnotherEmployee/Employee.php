<?php

declare(strict_types=1);

namespace App\AnotherEmployee;

class Employee
{
    public string $name;
    public int $age;
    public float $salary;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }

    /**
     * Check if age bigger than or eq 18
     *
     * @return bool
     */
    public function checkAge(): bool
    {
        if ($this->age >= 18) {
            return true;
        } else {
            return false;
        }
    }
}
