<?php

declare(strict_types=1);

namespace App\Towns;

class City
{
    public string $name;
    public string $foundation;
    public int $population;

    public function __construct(string $name, string $foundation, int $population)
    {
        $this->name = $name;
        $this->foundation = $foundation;
        $this->population = $population;
    }
}