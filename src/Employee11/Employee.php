<?php

declare(strict_types=1);

namespace App\Employee11;

class Employee
{
    public string $name;
    public int $age;
    public float $salary;

    /**
     * Employee constructor.
     *
     * @param string $name
     *
     * @param int $age
     *
     * @param float $salary
     */
    public function __construct(string $name, int $age, float $salary)
    {
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }
}