<?php

declare(strict_types=1);

namespace App\Employee9;

class Employee
{
    private string $name;
    private int $age;
    private float $salary;

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        if ($this->isAgeCorrect($age)) {
            $this->age = $age;
        }
    }

    /**
     * @param float $salary
     */
    public function setSalary(float $salary): void
    {
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getSalary(): string
    {
        return sprintf('%s $', $this->salary);
    }

    /**
     * @param $years
     *
     * @return bool
     */
    private function isAgeCorrect($years): bool
    {
        return ($years > 0 && $years <= 100) ? true : false;
    }
}
