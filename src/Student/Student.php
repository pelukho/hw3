<?php

declare(strict_types=1);

namespace App\Student;

class Student
{
    private const MAX_COURSE = 5;
    public string $name;
    public float $scholarship;
    public int $course;
    private string $courseAdministrator;

    /**
     * Transfer student to the next course
     * if it smaller or eq max course
     *
     * @param int $course
     */
    public function transferToNextCourse(int $course): void
    {
        if ($this->isCourseCorrect($course)) {
            $this->course = $course;
        }
    }

    /**
     * @param string $courseAdministrator
     */
    public function setCourseAdministrator(string $courseAdministrator): void
    {
        $this->courseAdministrator = $courseAdministrator;
    }

    /**
     * @return string
     */
    public function getCourseAdministrator(): string
    {
        return $this->courseAdministrator;
    }

    /**
     * @param float $scholarship
     */
    public function setScholarship(float $scholarship): void
    {
        $this->scholarship = $scholarship;
    }

    /**
     * @param int $course
     *
     * @return bool
     */
    private function isCourseCorrect(int $course): bool
    {
        return $course <= self::MAX_COURSE ? true : false;
    }
}
